const Koa = require("koa");
const Router = require("@koa/router");
const faunadb = require("faunadb");
const koaBody = require("koa-body");
var credentials = { name: 'viwat', pass: '123' }
const auth = require("koa-basic-auth")
const {
  Create,
  Collection,
  Get,
  Ref,
  Update,
  Delete,
  CreateIndex,
  Paginate,
  Match,
  Index,
  Documents,
  Map,
  Lambda,
  Var,
  Let,
  Select,
  Filter,
  CreateFunction,
  Query,
  Call,
  Function,
  CreateCollection,
} = require("faunadb");

const app = new Koa();
const router = new Router();
const faunaClient = new faunadb.Client({
  secret: "fnAEKCQEFeACBdg8NYSFkexxASb_kCJXWL-2Xq5x",
});

router.get("/planets", auth(credentials), async (ctx) => {
  let data = await faunaClient.query(
    Let(
      {
        planetDoc: Get(Ref(Collection("Planets"), "299732185556976133")),
      },
      {
        planet: Let(
          {},
          {
            id: Select(["ref", "id"], Var("planetDoc")),
            name: Select(["data", "name"], Var("planetDoc")),
            moons: Map(
              Select(["data", "moonRefs"], Var("planetDoc")),
              Lambda(
                "moonRef",
                Let(
                  {
                    moonDoc: Get(Var("moonRef")),
                  },
                  {
                    id: Select(["ref", "id"], Var("moonDoc")),
                    name: Select(["data", "name"], Var("moonDoc")),
                  }
                )
              )
            ),
          }
        ),
      }
    )
  );
  ctx.body = data;
});

router.get("/create", async (ctx) => {
  // faunaClient.query(
  //   Create(Collection("Sports"), {
  //     data: { name: "Football" }
  //   })
  // )
  // let data = await faunaClient.query(
  //   Get(Ref(Collection("Sports"), "300089918470750733"))
  // )

  // let data = await faunaClient.query(
  //   Paginate(Match(Index("all_Sports")))
  // )
  // let data = await faunaClient.query(
  //   Map(
  //     Paginate(Match(Index("all_Sports"))),
  //     Lambda('sportRef', Get(Var('sportRef')))
  //   )
  // )
  let data = await faunaClient.query(
    // Let(
    //   {
    //     sportDoc: Get(Ref(Collection("Sports"), "300089918470750733"))
    //   },
    //   {
    //     planet: Let({}, {
    //       id: Select(["ref", "id"], Var("sportDoc")),
    //       name: Select(["data", "name"], Var("sportDoc")),
    //       players: Map(
    //         Select(["data", "playerRefs"], Var("sportDoc")),
    //         Lambda("playerRef", Let(
    //           {
    //             playerDoc: Get(Var("playerRef"))
    //           },
    //           {
    //             id: Select(["ref", "id"], Var("playerDoc")),
    //             name: Select(["data", "name"], Var("playerDoc"))
    //           }
    //         ))
    //       )
    //     })
    //   }
    // )
    Call(Function("GetPlanetWithMoon"))
  )
  console.log(data)
  ctx.body = data
})

router.get("/sortplanets", async (ctx) => {
  let data = await faunaClient.query(
    faunadb.Paginate(Match(Index("all_Pilots_sorted_by_name_desc")))
  );
  ctx.body = data;
});

router.get("/get-movies-by-type", async (ctx) => {
  let data = await faunaClient.query(
    Map(
      Paginate(Match(Index("all_Movies_by_type"), ctx.query.type)),
      Lambda("movieRef", Get("movieRef"))
    )
  );
  ctx.body = data;
});


router.get("/pilots", async (ctx) => {
  let data = await faunaClient.query(
    Map(
      Paginate(Match(Index("all_Pilots"))),
      Lambda('pilotRef', Get(Var('pilotRef')))
    )
  )
  ctx.body = data;
})

router.post("/pilots", koaBody(), (ctx) => {
  faunaClient.query(
    Create(Collection("Pilots"), {
      data: ctx.request.body,
    })
  );
  ctx.body = ctx.request.body;
});

router.get("/pilots/:id", async (ctx) => {
  let data = await faunaClient.query(
    Get(Ref(Collection("Pilots"), ctx.params.id))
  );
  ctx.body = data;
});

router.put("/pilots/:id", koaBody(), async (ctx) => {
  faunaClient.query(
    Update(Ref(Collection("Pilots"), ctx.params.id), {
      data: ctx.request.body,
    })
  );
  ctx.body = "update Successfully";
});

router.delete("/pilots/:id", async (ctx) => {
  faunaClient.query(Delete(Ref(Collection("Pilots"), ctx.params.id)));
  ctx.body = "Delete document successfully";
});

/** warehouses */
router.get("/warehouses", async (ctx) => {
  let q = await faunaClient.query(
    faunadb.Map(
      faunadb.Paginate(faunadb.Documents(faunadb.Collection("warehouses"))),
      faunadb.Lambda((warehouses) => faunadb.Get(warehouses))
    )
  );

  let results = q.data.map((item) => {
    return item.data;
  });

  q.data = results;

  ctx.status = 200;
  ctx.body = q;
});

//post
router.post("/warehouses", koaBody(), (ctx) => {
  faunaClient.query(
    faunadb.Create(faunadb.Collection("warehouses"), {
      data: ctx.request.body,
    })
  );
  ctx.body = ctx.request.body;
});

//get by ref
router.get("/warehouses/:ref", async (ctx) => {
  faunaClient
    .query(
      faunadb.Get(faunadb.Ref(faunadb.Collection("warehouses"), ctx.params.ref))
    )
    .then((ret) => console.log(ret));
});

//update by ref
router.put("/warehouses/:ref", async (ctx) => {
  faunaClient.query(
    faunadb
      .Update(faunadb.Ref(faunadb.Collection("warehouses"), ctx.params.ref), {
        data: ctx.request.body,
      })
      .then((ret) => console.log(ret))
  );
});

//delete
router.delete("/warehouses/:ref", async (ctx) => {
  faunaClient
    .query(
      faunadb.Delete(
        faunadb.Ref(faunadb.Collection("warehouses"), ctx.params.ref)
      )
    )
    .then((ret) => console.log(ret));
});

app.use(router.routes());
app.use(function* (next) {
  try {
    yield next;
  } catch (err) {
    if (401 == err.status) {
      this.status = 401;
      this.set('WWW-Authenticate', 'Basic');
      this.body = 'You have no access here';
    } else {
      throw err;
    }
  }
});

app.listen(3000, (err) => {
  if (err) {
    throw err;
  }
});
